package rockPaperScissors;

import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

public class RockPaperScissors {
	
	public static void main(String[] args) {
    	/* 	
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */
        new RockPaperScissors().run();
        
    }
    
    
    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");
    
    public void run() {
        // TODO: Implement Rock Paper Scissors
    	System.out.println("Let's play round " + roundCounter);
    	roundCounter++;
    	play();
    }
    public void play() {
    	String humchoice = readInput("Your choice (Rock/Paper/Scissors)?");
    	String compchoice = rpsChoices.get(new Random().nextInt(rpsChoices.size()));
    	//System.out.println("hei" + humchoice);
    	
       	if(rpsChoices.contains(humchoice)) {
    	//if(choice.equals(rpsChoices.get(0)) || choice.equals(rpsChoices.get(1))|| choice.equals(rpsChoices.get(2)) ) {
    		System.out.println("Human chose " + humchoice + ", computer chose " + compchoice + ". " + winResult(humchoice, compchoice) );
    		System.out.println("Score: human " + humanScore + ", computer " + computerScore);
    	}
    	else {
    		System.out.println("I do not understand " + humchoice + ". Could you try again?");
    		play();
    	}
    	
    	String answer = readInput("Do you wish to continue playing? (y/n)?");
    	if(answer.equals("y")) {
    		run();
    	}
    	
    	else{
    		System.out.println("Bye bye :)");
    	}
    }
    
    
    
    public String winResult(String a, String b) {
    	if (a.equals(b)) {
        	return "It's a tie!";
    	}
    	else if (a.equals("rock") && b.equals("scissors")){
    		humanScore++;
    		return "Human wins!";
    	}
    	else if (a.equals("rock") && b.equals("paper")) {
    		computerScore++;
    		return "Computer wins!";
    	}
    	else if(a.equals("paper") && b.equals("rock")) {
    		humanScore++;
    		return "Human wins!";
    	}
    	else if(a.equals("paper") && b.equals("scissors")) {
    		computerScore++;
    		return "Computer wins!";
    	}
    	else if(a.equals("scissors") && b.equals("paper")) {
    		humanScore++;
    		return "Human wins!";
    	}
    	else if(a.equals("scissors") && b.equals("rock")){
    		computerScore++;
    		return "Computer wins!";
    	}
		return null;
   
    }
    
    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }

}
